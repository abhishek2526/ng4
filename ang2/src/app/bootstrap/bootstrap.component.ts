import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bootstrap',
  templateUrl: './bootstrap.component.html',
  styleUrls: ['./bootstrap.component.css']
})
export class BootstrapComponent implements OnInit {

  // For if then example
  courses: number[] = [];

  // for Switch Example
  viewMode = 'map';

  // For for loop
  list: any[] = [
    {
      id: 1,
      name: 'PHP'
    },
    {
      id: 2,
      name: 'ANGULAR JS'
    },
    {
      id: 3,
      name: 'NODE JS'
    },
    {
      id: 4,
      name: 'JAVA'
    }
  ];

  constructor() { }

  ngOnInit() {
  }


  onAdd(){
    this.list.push({
      id: 4,
      name: 'REACT JS',
    });
  }

  removeCourse(lists){
    let index = this.list.indexOf(lists);
    this.list.splice(index ,1);
  }

}
