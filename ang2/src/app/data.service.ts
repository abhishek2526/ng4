import { Injectable } from '@angular/core';


//It is used to to emit metadata associated with this service for dependency
@Injectable()
export class DataService {

  constructor() { }

  cars = [
    'ford','audi','bmw','ferrari'
  ];

  myData(){
    return 'This is my data man!';
  }

}
