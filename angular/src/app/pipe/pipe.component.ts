import { Component, OnInit } from '@angular/core';
import { SortPipe } from './pipe';


@Component({
  selector: 'app-pipe',
  templateUrl: './pipe.component.html',
  styleUrls: ['./pipe.component.css'],
})
export class PipeComponent implements OnInit {
  day = new Date(1992, 3, 15);
  array = [12, 23, 434, 54, 65, 767, 45,32, 23]; 
  samples: Array<object> = 
  [
    {
      color: "red",
      value: "#f00"
    },
    {
      color: "green",
      value: "#0f0"
    },
    {
      color: "blue",
      value: "#00f"
    },
    {
      color: "cyan",
      value: "#0ff"
    },
    {
      color: "magenta",
      value: "#f0f"
    },
    {
      color: "yellow",
      value: "#ff0"
    },
    {
      color: "black",
      value: "#000"
    }
  ];

  constructor() { }

  ngOnInit() {

  }

}
