import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {
  @Output() notify = new EventEmitter();
  @Output() newNotify = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  emitNotify(){
    this.notify.emit('i just emitted a data');
  }

  emitNewNotify(){
    this.notify.emit('Just emitted a new data data');
  }

}
