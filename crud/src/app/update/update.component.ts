import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {
  users: any = localStorage.getItem('users');
  current_user: any;
  id: number;
  success:boolean = true;
  constructor(private router : Router, private route: ActivatedRoute) {
    this.users = (this.users) ? JSON.parse(this.users) : [];
    this.current_user = this.users[0];
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
        if(!params.id || params.id >= this.users.length || params.id < 0){
            this.router.navigate(['/list']);
        }
        this.id = params.id;
        this.current_user = this.users[params.id];
      });
  }

  onUpdate(value: any){
    console.log(this.users);
    console.log(this.id);
    this.users[this.id] = value;
    console.log(this.users);
    localStorage.setItem('users',JSON.stringify(this.users));
    this.success = false;
  }

}
