import { animate, trigger, state, keyframes, style, transition } from '@angular/animations';

export let fade = trigger('fade',[
    transition('void => *',[
    style({
        opacity: 0,
        transform: 'translateY(20px)'
    }),
    animate(600)
    ]),
    transition('* => void',[
    animate(500,keyframes([
        style({ opacity: 1, transform: 'translateX(0px)' }),
        style({ opacity: 0, transform: 'translateX(500px)' }),
        ]))
    ])
]); 