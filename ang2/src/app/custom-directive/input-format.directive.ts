import { Directive, HostListener, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[appInputFormat]'
})
export class InputFormatDirective {

  @Input('appInputFormat') appInputFormat;

  constructor(private el: ElementRef) {
  }

  @HostListener('focus') onFocus(){
    let value = this.el.nativeElement.value;
    this.el.nativeElement.value = value.toLowerCase();
    console.log(this.appInputFormat);
  }

  @HostListener('blur') onBlur(){
    let value = this.el.nativeElement.value;
    this.el.nativeElement.value = value.toUpperCase();
    console.log(this.appInputFormat);
  }

}
