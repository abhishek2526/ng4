import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CourseService } from './boiler.service';


@Component({
  selector: 'app-boiler',
  templateUrl: './boiler.component.html',
  styleUrls: ['./boiler.component.css'],
  // encapsulation: ViewEncapsulation.None, It makes the element to behave like a normal as all inner and outer css applies to it
  // encapsulation: ViewEncapsulation.Emulated,  Default + No Css of this Component Goes Outside
  // encapsulation: ViewEncapsulation.Native No Css of outer Components or Global Css applies to this
})
export class BoilerComponent implements OnInit {
  courses: string[];
  text:string = ' Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.';
  list: any[];


  course = {
    title : "The complete site",
    rating: 4.345465,
    students: 5456450,
    price: 235.54,
    releseDate: new Date(2016, 3, 1)
  }

  loadCourse(){
    this.list = [{
      id: 1,
      name: 'PHP'
    },
    {
      id: 2,
      name: 'ANGULAR JS'
    },
    {
      id: 3,
      name: 'NODE JS'
    },
    {
      id: 4,
      name: 'JAVA'
    }];
  }

  trackList(index,list){
      return list ? list.id : undefined;
  }

  constructor(service: CourseService) {
    this.courses = service.getList();
  }

  ngOnInit() {
  }

}
