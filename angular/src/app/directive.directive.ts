import { Directive, ElementRef } from '@angular/core';

@Directive({
   selector: '[bg-yellow]'
})

export class BgYelloWDirective{
  constructor(el: ElementRef){
    el.nativeElement.style.background = 'yellow';
  }
}

@Directive({
  selector: '[bg-green]'
})

export class BgGreenDirective{
 constructor(el: ElementRef){
   el.nativeElement.style.background = 'green';
 }
}

@Directive({
  selector: '[bg-red]'
})

export class BgRedDirective{
 constructor(el: ElementRef){
   el.nativeElement.style.background = 'red';
 }
}

@Directive({
  selector: '[bg-blue]'
})

export class BgBlueDirective{
 constructor(el: ElementRef){
   el.nativeElement.style.background = 'blue';
 }
}