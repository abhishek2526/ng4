"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var CssComponent = /** @class */ (function () {
    function CssComponent() {
        this.title = 'The Title of Css';
        this.name = 'The Title of Name';
        this.success = true;
    }
    CssComponent.prototype.ngOnInit = function () {
    };
    CssComponent = __decorate([
        core_1.Component({
            // selector: '[app-css]',
            selector: '.app-css',
            templateUrl: './css.component.html',
            styleUrls: ['./css.component.css']
        })
    ], CssComponent);
    return CssComponent;
}());
exports.CssComponent = CssComponent;
