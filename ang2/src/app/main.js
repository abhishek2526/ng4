"use strict";
// // #011 Example
//
// function log(message){
//   console.log(message);
// }
//
//
// var message = 'Hello world';
// log(message);
//
//
// // #012 Example
// // Differnce between let and var
// //LET - It's Scope is Just within the code block in which it is created.
// //LET - It's Scope is Just within the parent function block in which it is created.
//
//
// function doSomething(){
//     for(var i=0;i<5;i++){
//       console.log(i);
//   }
//   console.log('val is ' + i);
// }
//
// doSomething();
//
//
//
// // #013 Example
// let count;
// count = true;
// count = 5;
// count = 'This is it';
//
//
// let a: number = 5;
// let b: boolean;
// let c: string;
// let d: any;
// let e: number[] = [5,4,3,2,1];
// let f: any[] = [5,true,'this',44,false];
// let g: string[] = ['5','4','3','2','1'];
//
// const Redcolor = '000';
//
// enum Color {
//   Red = 0,
//   Green = 1,
//   Blue = 2,
//   Yellow = 3,
//   Greem = 4
//
// };
// console.log('The color is : ' + Color.Green)
//
// // #014 Type Assertions Example
//
// let msg;
// msg = 'This is a simple message!';
// msg = (<string>message).bold;
// msg = (message as string).bold;
//
//
// // #015 Arrow Functions Example
//
// let text = 'This is a simple Text Message';
// let writeText = (text)=>{
//   console.log(text);
// }
//
//
// // #016 Interfaces Example
// // Using Inline Annotations
//
// let drawPoint = (point:{ x:number, y:number })=>{
//   console.log('value of x is : ' + this.x + ' and value of x is : ' + this.y);
// }
//
// interface Point{
//     x: number,
//     y: number
// }
//
// let drawLine = (point: Point)=>{
//   console.log('The value is : ' + point.x);
// }
//
exports.__esModule = true;
// // #017 Typescript Classes Example
// class Point{
//   x: number;
//   y: number;
//
// //? means it is an optional parameter
//   constructor(x?:number, y?:number){
//       this.x = x;
//       this.y = y;
//   }
//
//   draw(){
//     console.log('Value of x is : ' + this.x + ' and Value of y is : ' + this.y )
//   }
// }
//
//
// let point = new Point(2);
// point.draw();
// // #020 Typescript Properties Example
// class Point{
//
//   constructor(private x?:number,private y?:number){
//   }
//
//   draw(){
//     console.log('Value of x is : ' + this.x + ' and Value of y is : ' + this.y )
//   }
//
//   get X(){
//     return this.x;
//   }
//
//   set X(value){
//     if(value < 0)
//       throw new Error('Value can\'t be negative');
//
//     this.x = value;
//   }
// }
//
//
// let point = new Point(1,2);
// let x = point.X;
// point.X = 10;
// point.draw();
// // #023 Typescript Modules Example
// import { Point } from './point';
// let point = new Point(1,2);
// let x = point.X;
// point.X = 10;
// point.draw();
// // #025 Typescript Modules Example
var like_component_1 = require("./like.component");
var component = new like_component_1.LikeComponent(10, true);
component.onClick();
console.log("likesCount : " + component.likesCount + ", isSelected : " + component.isSelected + " ");
