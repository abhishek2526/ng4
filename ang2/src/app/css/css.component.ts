import { Component, OnInit } from '@angular/core';

@Component({
  // selector: '[app-css]',
  selector: '.app-css',
  templateUrl: './css.component.html',
  styleUrls: ['./css.component.css']
})
export class CssComponent implements OnInit {

  title = 'The Title of Css';
  name = 'The Title of Name';
  success = true;

  constructor() { }

  ngOnInit() {
  }

}
