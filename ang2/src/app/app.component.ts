import { Component } from '@angular/core';
import { Bike } from './spec';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  isFav: boolean = true;
  post = {
    title: 'Thug Life',
    author: 'King Aurthor'
  }

  onOutputChanged(data){
    // console.log('Output Changed' , data);
  }

   ram_car:car = new car();
   ram_bike:Bike = new Bike();
   abhi_bike:Bike = new Bike();
   size: number = 15;

  constructor(){
      // this.ram_bike.company = 'HERO HONDA';
      // this.abhi_bike.company = 'BAJAJ';
  }

  ngOnInit(){
    this.abhi_bike.company = 'BAJAJ';
    // console.log(this.ram_bike);
    // console.log(this.abhi_bike);

  }
}

// To get a sample class from the same component
class car{
  wheels: number = 4;
  company: string = 'AUDI';
  mirror: boolean = true;
  info: any = {
      id:  20112,
      gstIn:  'T3YT34FG',
      spec: ['front','back','top','rear'],
      agent: {
        fname: 'Abhishek',
        lname: 'Kumar',
        eId: 'ECDS543576',
        pic: 'gif.jpg'
      }
  };
}
