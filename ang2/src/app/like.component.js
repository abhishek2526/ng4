"use strict";
exports.__esModule = true;
var LikeComment = /** @class */ (function () {
    function LikeComment(likesCount, isSelected) {
        this.likesCount = likesCount;
        this.isSelected = isSelected;
    }
    LikeComment.prototype.onClick = function () {
        this.likesCount += (this.likesCount) ? 1 : -1;
        this.isSelected != this.isSelected;
    };
    return LikeComment;
}());
exports.LikeComment = LikeComment;
