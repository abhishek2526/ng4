import { Component, OnInit } from '@angular/core';
import { Recipie } from '../recipie.model';

@Component({
  selector: 'app-recipie-list',
  templateUrl: './recipie-list.component.html',
  styleUrls: ['./recipie-list.component.css']
})
export class RecipieListComponent implements OnInit {
  recipies: Recipie[] = [
    new Recipie('Samosa','this is simpoly a test maybe','http://www.indianfoodforever.com/images/rainy-recipes.jpg'),
    new Recipie('Samosa','this is simpoly a test maybe','http://www.indianfoodforever.com/images/rainy-recipes.jpg'),
    new Recipie('Samosa','this is simpoly a test maybe','http://www.indianfoodforever.com/images/rainy-recipes.jpg')
  ];

  constructor() { }

  ngOnInit() {
  }

}
