import { Pipe,PipeTransform } from '@angular/core';

@Pipe({
    name: 'currency'
})

export class Currency{
    transform(value){
        let array: Array<any> = [];
        value.forEach(element => {
            array.push('Rs.' + element*60);
        });
        return array;
    }
}

@Pipe({
    name: 'dumb'
})

export class Dumb{
    transform(value){
        let array: Array<any> = [];
        value.forEach(element => {
            array.push('Dumb.' + element*60);
        });
        return array;
    }
}

@Pipe({
    name: 'test'
})

export class Test{
    transform(value){
        let array: Array<any> = [];
        value.forEach(element => {
            array.push('Test.' + element*60);
        });
        return array;
    }
}

@Pipe({
    name: 'toast'
})

export class Toast{
    transform(value){
        let array: Array<any> = [];
        value.forEach(element => {
            array.push('Toast.' + element*60);
        });
        return array;
    }
}