import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss']
})
export class NewUserComponent implements OnInit {
  users: any = localStorage.getItem('users');
  success: boolean = true;
  constructor(private router: Router){
    this.users = (this.users) ? JSON.parse(this.users) : [];
  }

  ngOnInit() {
  }

  onSubmit(value: any){
    this.users.push(value);
    localStorage.setItem('users',JSON.stringify(this.users));
    this.success = false;
  }

}
