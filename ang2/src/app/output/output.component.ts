import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-output',
  templateUrl: './output.component.html',
  styleUrls: ['./output.component.css']
})
export class OutputComponent implements OnInit {
  @Output() changeOutput = new EventEmitter();
  data:any = {
      fname: 'Abhishek',
      lname: 'Kumar'
  };


  constructor() { }

  ngOnInit() {
  }

  onClick(data){
    console.log('Clicked' , this.data);
    this.changeOutput.emit(this.data);
  }

}
