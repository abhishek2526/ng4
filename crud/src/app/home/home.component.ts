import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }
  users: any[] = [];

  ngOnInit() {
    if(localStorage.getItem('users') === null){
        localStorage.setItem('users',JSON.stringify(this.users));
        console.log('Setting the localStorage item as : ' + localStorage.getItem('users'));

    }
  }

}
