import { trigger,transition,style,animate,state, keyframes } from '@angular/animations';

export let fade = trigger('fade',[
                        state('void', style({
                        transform: 'translateY(50px)',
                        opacity: 0
                        })),      
                        transition('void <=> *', [
                        animate(1000)
                        ])
                    ]);




export let rotate = trigger('rotate',[
                        state('void', style({
                        transform: 'rotate(-360deg)',
                        opacity: 0
                        })),
                        transition('void <=> *',[
                        animate(2000)
                        ])
                    ]);
                    
                    
export let custom = trigger('custom',[
                        transition('void => *',[
                            animate(1000, keyframes([
                                style({ opacity: 0, transform: 'translate(0px,30px)', offset: 0 }),
                                style({ opacity: 0.5, transform: 'translate(0px,15px)',  offset: 0.3 }),
                                style({ opacity: 1, transform: 'translate(0px,0px)', offset: 1 })
                            ]))
                        ]),
                        transition('* => void',[
                            animate(500, keyframes([
                                style({ opacity: 1, transform: 'translate(0px,0px)' }),
                                style({ opacity: 0, transform: 'translate(-50px,0px)' })
                            ]))
                        ])
                    ]);