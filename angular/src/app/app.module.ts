import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ParentComponent } from './output/parent/parent.component';
import { ChildComponent } from './output/parent/child/child.component';
import { ServerComponent } from './server/server.component';
import { RecipiesComponent } from './recipies/recipies.component';
import { RecipieDetailComponent } from './recipies/recipie-detail/recipie-detail.component';
import { RecipieListComponent } from './recipies/recipie-list/recipie-list.component';
import { RecipieItemComponent } from './recipies/recipie-list/recipie-item/recipie-item.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { ShoppingEditComponent } from './shopping-list/shopping-edit/shopping-edit.component';
import { PipeComponent } from './pipe/pipe.component';
import { SortPipe } from './pipe/pipe';
import { Currency, Dumb, Test, Toast } from './pipe/currency';
import { BgYelloWDirective,BgGreenDirective,BgBlueDirective,BgRedDirective } from './directive.directive';
import { AnimationComponent } from './animation/animation.component';

@NgModule({
  declarations: [
    //Declare pipes,directives at the top of the app declarations so as to get it known to the below following components  
    Dumb,Test,Toast,
    SortPipe,
    Currency,
    AppComponent,
    HeaderComponent,
    RecipiesComponent,
    RecipieDetailComponent,
    RecipieListComponent,
    RecipieItemComponent,
    ShoppingListComponent,
    ShoppingEditComponent,
    PipeComponent,
    BgYelloWDirective,
    BgGreenDirective,
    BgBlueDirective,
    BgRedDirective,
    AnimationComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
