import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { AppComponent } from './app.component';
import { CssComponent } from './css/css.component';
import { AlertModule } from 'ng2-bootstrap/ng2-bootstrap';
import { NewCompComponent } from './new-comp/new-comp.component';
import { BoilerComponent } from './boiler/boiler.component';
import { CourseService } from './boiler/boiler.service';
import { SummaryPipe } from './boiler/boiler.pipe';
import { BootstrapComponent } from './bootstrap/bootstrap.component';
import { CustomDirectiveComponent } from './custom-directive/custom-directive.component';
import { InputFormatDirective } from './custom-directive/input-format.directive';
import { NgFormComponent } from './ng-form/ng-form.component';
import { RouterModule } from '@angular/router';
import { HttpComponent } from './http/http.component';
import { HttpModule } from '@angular/http';
import { InputComponent } from './input/input.component';
import { OutputComponent } from './output/output.component';
import { SampleComponent } from './sample/sample.component';
import { BackSlash } from './sample/pipe';
import { bgDirective,bgRedDirective } from './sample/directive';
import { DataService } from './data.service';
import { DataServices } from '../services/app.service';
import { WeatherComponent } from './weather/weather.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
  declarations: [
    bgRedDirective,
    bgDirective,
    BackSlash,
    AppComponent,
    CssComponent,
    NewCompComponent,
    BoilerComponent,
    SummaryPipe,
    BootstrapComponent,
    CustomDirectiveComponent,
    InputFormatDirective,
    NgFormComponent,
    HttpComponent,
    InputComponent,
    OutputComponent,
    SampleComponent,
    WeatherComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    InfiniteScrollModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: BoilerComponent },
      { path: 'form', component: NgFormComponent },
      { path: 'http', component: HttpComponent },
      { path: 'input', component: InputComponent },
      { path: '**', component: BootstrapComponent },
    ]),
    AlertModule.forRoot()
  ],
  providers: [
    CourseService,
    DataService,
    DataServices
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
