import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {
  @Input('isFav') isFavourite:boolean;
  @Input('data') user_data: any;
  @Input('ram-car') car:any;

  constructor() { }

  ngOnInit() {
  }

}
