import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {
  data: any;
  constructor() { }

  ngOnInit() {
  }

  gotNotification(ev){
    // console.log(ev);
    console.log('m called');
    this.data = ev;
  }

}
