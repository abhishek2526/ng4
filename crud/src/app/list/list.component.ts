import { Component, OnInit } from '@angular/core';
import { UserList } from '../userlist';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  users : any;
  constructor(private router: Router) { }

  ngOnInit() {
    this.users = localStorage.getItem('users');
    this.users = (this.users) ? JSON.parse(this.users) : [];
    if(!(this.users && this.users.length > 0)){
      this.router.navigate(['/create']);
    }
  }

  deleteUser(i){
    this.users.splice(i, 1);
    localStorage.setItem('users', JSON.stringify(this.users));
    if(this.users.length == 0){
      this.router.navigate(['/create']);
    }
  }
}
