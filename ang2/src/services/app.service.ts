import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class DataServices{
 constructor(private http: Http){
 }

 getData(){
    this.http.get('https://jsonplaceholder.typicode.com/comments')
    .subscribe(
      (response)=>{
        console.log(response);
        return response._body.json();
      }
    );
 }
}