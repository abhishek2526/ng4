export class Bike{
  wheels: number = 2;
  company: string = 'YAMAHA';
  mirror: boolean = true;
  info: any = {
      id:  'ED112',
      gstIn:  '185T34FG',
      agent: [{
        fname: 'Abhishek',
        lname: 'Kumar',
        eId: 'HGJHK5789',
        pic: 'abhi.png'
      },{
        fname: 'Akhilesh',
        lname: 'Khajuria',
        eId: 'RDS75789',
        pic: 'akhilesh.png'
      },{
        fname: 'Yajya',
        lname: 'Kalra',
        eId: 'YAHJY75789',
        pic: 'yajya.png'
      },{
        fname: 'Monika',
        lname: 'Pal',
        eId: 'PAL75789',
        pic: 'monika.png'
      },{
        fname: 'Avinash',
        lname: 'Chand',
        eId: 'AVI75789',
        pic: 'avinash.png'
      },{
        fname: 'Ashish',
        lname: 'Kumar',
        eId: 'AHHSI5789',
        pic: 'ashish.png'
      }]
  };
}
