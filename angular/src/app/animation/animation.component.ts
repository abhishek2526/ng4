import { Component, OnInit } from '@angular/core';
import { fade,rotate,custom } from '../animations';


@Component({
  selector: 'app-animation',
  templateUrl: './animation.component.html',
  styleUrls: ['./animation.component.css'],
  animations: [
    custom
  ]
})
export class AnimationComponent implements OnInit {
  users: Array<string> = ['Abhi'];

  constructor() { }

  ngOnInit() {
  }

  addUser(value){
    this.users.push(value);
  }

  removeUser(i){
    this.users.splice(i, 1);
  }

}
