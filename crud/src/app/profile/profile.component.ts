import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  users: any = localStorage.getItem('users');
  current_user: any;
  constructor(private router : Router, private route: ActivatedRoute) {
    this.users = (this.users) ? JSON.parse(this.users) : [];
    this.current_user = this.users[0];
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
        if(!params.id || params.id >= this.users.length || params.id < 0){
            this.router.navigate(['/list']);
        }
        this.current_user = this.users[params.id];
      });
  }

}
