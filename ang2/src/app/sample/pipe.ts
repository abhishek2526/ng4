import { Pipe,PipeTransform } from '@angular/core';

@Pipe({
    name: 'removeBackslash'
})

export class BackSlash{
    transform(value){
        value = value.replace(/\//g, "");     //here /g is a parameter showing to remove all the instances from the string
        value = value.replace(/a/g, "");      //remove all the instances of a from the string
        return value;
    }
}