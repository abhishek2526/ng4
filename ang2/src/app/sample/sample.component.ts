import { Component, OnInit } from '@angular/core';
import { animate, trigger, keyframes, style, state } from '@angular/animations';
import { fade } from './animation';
import { DataService } from '../data.service';

@Component({
  selector: 'app-sample',
  templateUrl: './sample.component.html',
  styleUrls: ['./sample.component.css'],
  animations: [
    fade
  ]
})

export class SampleComponent implements OnInit {
  heading: string;
  users: Array<string> = ['Abhishek'];

  // Use service via dependancy injection
  constructor(private dataService: DataService) { }

  ngOnInit() {
    console.log(this.dataService.cars);
    this.heading = this.dataService.myData();
  }

  addUser(value){
    this.users.push(value);
  }

  removeUser(index){
    this.users.splice(index,1);
  }

}
