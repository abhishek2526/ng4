import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})

export class ChildComponent implements OnInit {
  @Output('newName') notify = new EventEmitter();
  data: any = {
    fname: 'Abhishek',
    lname: 'Kumar',
    empId: 112043,
    company: 'Mogli Labs Pvt. LTD'
  };
  constructor() { }

  ngOnInit() {
    // this.notify.emit('Lets\'s emit data');
    // this.notify.emit(this.data);
  }

  childBtn(){
    this.notify.emit(this.data);
  }
}
