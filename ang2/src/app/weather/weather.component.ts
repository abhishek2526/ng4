import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { fade } from '../animations';
import { DataServices } from '../../services/app.service';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css'],
  animations: [
    fade
  ]

})
export class WeatherComponent implements OnInit {
  title:string = "This is a weather App";
  cityName:string;
  data: Array<object>;
  bucket: Array<object>;

  constructor(private http: Http, private dataServices: DataServices) { }

  ngOnInit(){
  }

  onScroll(){
    // if(!this.bucket){
    //   return;
    // }
    // let array = this.bucket.slice(this.data.length,this.data.length +5);
    // console.log(array);
    // for(let i=0;i<array.length;i++){
    //   console.log(array[i]);
    //   this.data.push(array[i]);
    // }
    // this.data.push(this.bucket.slice(this.data.length,this.data.length + 1)[0]);
  }

  searchCity(){
    console.log(this.dataServices.getData());
  }

}
