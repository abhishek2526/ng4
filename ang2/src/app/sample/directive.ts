import { Directive, ElementRef } from '@angular/core';

@Directive({
    selector: '[bgColor]'
})

export class bgDirective{
    
  constructor(el: ElementRef){
    el.nativeElement.style.background = 'yellow';
    el.nativeElement.style.padding = '50px';
  }

}


@Directive({
    selector: '[bgRedColor]'
})

export class bgRedDirective{
    
  constructor(el: ElementRef){
    el.nativeElement.style.background = 'red';
  }
  
}