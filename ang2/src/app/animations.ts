import { animate, state, trigger, style, transition } from '@angular/animations';

export let fade = trigger('fade',[
    transition('void => *',[
    style({
        opacity: 0,
        transform: 'translateY(30px)'
    }),
    animate(600,
    style({
        opacity: 1,
        transform: 'translateY(0px)'
    }))
    ])
]);