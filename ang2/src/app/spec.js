"use strict";
exports.__esModule = true;
var Bike = /** @class */ (function () {
    function Bike() {
        this.wheels = 2;
        this.company = 'YAMAHA';
        this.mirror = true;
        this.info = {
            id: 'ED112',
            gstIn: '185T34FG',
            agent: [{
                    fname: 'Abhishek',
                    lname: 'Kumar',
                    eId: 'HGJHK5789',
                    pic: 'abhi.png'
                }, {
                    fname: 'Akhilesh',
                    lname: 'Khajuria',
                    eId: 'RDS75789',
                    pic: 'akhilesh.png'
                }, {
                    fname: 'Yajya',
                    lname: 'Kalra',
                    eId: 'YAHJY75789',
                    pic: 'yajya.png'
                }, {
                    fname: 'Monika',
                    lname: 'Pal',
                    eId: 'PAL75789',
                    pic: 'monika.png'
                }, {
                    fname: 'Avinash',
                    lname: 'Chand',
                    eId: 'AVI75789',
                    pic: 'avinash.png'
                }, {
                    fname: 'Ashish',
                    lname: 'Kumar',
                    eId: 'AHHSI5789',
                    pic: 'ashish.png'
                }]
        };
    }
    return Bike;
}());
exports.Bike = Bike;
