"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var spec_1 = require("./spec");
var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.ram_car = new car();
        this.ram_bike = new spec_1.Bike();
        this.abhi_bike = new spec_1.Bike();
        this.size = 15;
        // this.ram_bike.company = 'HERO HONDA';
        // this.abhi_bike.company = 'BAJAJ';
    }
    AppComponent.prototype.ngOnInit = function () {
        this.abhi_bike.company = 'BAJAJ';
        console.log(this.ram_bike);
        console.log(this.abhi_bike);
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.css']
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
// To get a sample class from the same component
var car = /** @class */ (function () {
    function car() {
        this.wheels = 4;
        this.company = 'AUDI';
        this.mirror = true;
        this.info = {
            id: 20112,
            gstIn: 'T3YT34FG',
            spec: ['front', 'back', 'top', 'rear'],
            agent: {
                fname: 'Abhishek',
                lname: 'Kumar',
                eId: 'ECDS543576',
                pic: 'gif.jpg'
            }
        };
    }
    return car;
}());
