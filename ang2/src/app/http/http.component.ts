import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';

@Component({
  selector: 'app-http',
  templateUrl: './http.component.html',
  styleUrls: ['./http.component.css']
})
export class HttpComponent implements OnInit {
  posts: any[];
  private url: string = 'https://jsonplaceholder.typicode.com/posts';
  constructor(private http: Http) {
    http.get(this.url).subscribe(response => {
      console.log(response.json());
      this.posts = response.json();
    })
  }

  ngOnInit() {
  }

  createPost(input: HTMLInputElement){
    let post = {title: input.value}
    this.http.post(this.url,JSON.stringify(post)).subscribe(response => {
      console.log(response);
    })
  }

}
